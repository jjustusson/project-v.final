package com.hm;

/*
    Student names:  Andrew Brenycz
                    Julia Justusson
                    Robert Perez
                    Brittani Vinson

    Assignment:     FINAL PROJECT [File 5/5: HangmanGraphics]
 */

public class HangmanGraphics {

    public static void main(String[] args) { }

    /**
     * Displays appropriate hangman image, based on user progress in the game.
     *
     * @param attempts -- the number of letter guess attempts the user has remaining
     */
    public static void drawMan(int attempts)
    {
        // String representations of hangman drawing, for instances from
        //  remaining attempts = 5 (no incorrect guesses made) to
        //  remaining attempts = 0 (five incorrect guesses made, game is over: user lost)

        String[] s5 = { " ______  ",
                        "|      |  ",
                        "|      | ",
                        "|     ",
                        "|   ",
                        "|      ",
                        "|     " };

        String[] s4 = { " ______  ",
                        "|      |  ",
                        "|      | ",
                        "|     ( )",
                        "|   ",
                        "|      ",
                        "|     " };

        String[] s3 = { " ______  ",
                        "|      |  ",
                        "|      |",
                        "|     ( )",
                        "|      | ",
                        "|      |",
                        "|         " };

        String[] s2 = { " ______  ",
                        "|      |  ",
                        "|      |",
                        "|     ( )",
                        "|   ___|___ ",
                        "|      |",
                        "|         " };

        String[] s1 = { " ______  ",
                        "|      |  ",
                        "|      |",
                        "|     ( )",
                        "|   ___|___ ",
                        "|      |",
                        "|     /    " };

        String[] s0 = { " ______  ",
                        "|      |  ",
                        "|      |",
                        "|     ( )",
                        "|   ___|___ ",
                        "|      |",
                        "|     / \\      GAME OVER!" };


        // Methods to determine # attempts remaining & appropriate hangman string representation to display

        if(attempts == 5) {
            for (int i = 0; i < s5.length; ++i) { //if t is not as long as s, this will throw an exception
                System.out.print(s5[i]);
                System.out.println();
            }
        }

        else if (attempts == 4) {
            for (int i = 0; i < s4.length; ++i) { //if t is not as long as s, this will throw an exception
                System.out.print(s4[i]);
                System.out.println();
            }
        }

        else if (attempts == 3) {
            for (int i = 0; i < s3.length; ++i) { //if t is not as long as s, this will throw an exception
                System.out.print(s3[i]);
                System.out.println();
            }
        }

        else if (attempts == 2)
        {
            for (int i = 0; i < s2.length; ++i) { //if t is not as long as s, this will throw an exception
                System.out.print(s2[i]);
                System.out.println();
            }
        }

        else if (attempts == 1) {
            for (int i = 0; i < s1.length; ++i) { //if t is not as long as s, this will throw an exception
                System.out.print(s1[i]);
                System.out.println();
            }
        }

        else if (attempts == 0) {
            for (int i = 0; i < s0.length; ++i) { //if t is not as long as s, this will throw an exception
                System.out.print(s0[i]);
                System.out.println();
            }
        }
    }
}

