package com.hm;

/*
    Student names:  Andrew Brenycz
                    Julia Justusson
                    Robert Perez
                    Brittani Vinson

    Assignment:     FINAL PROJECT [File 2/5: Server]
*/


import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {

    /**
     * Server: Listens for and connects to client.
    **/
    public static void main(String[] args) {

        Hangman hang = null;

        try {

            ServerSocket s1 = new ServerSocket(1236);
            Socket socket = s1.accept();

            ObjectInputStream serverInputStream = new
                    ObjectInputStream(socket.getInputStream());

            ObjectOutputStream serverOutputStream = new
                    ObjectOutputStream(socket.getOutputStream());

            hang = (Hangman)serverInputStream.readObject();
            hang.getWord();
            hang.getDashed();

            serverInputStream.close();
            serverOutputStream.close();
            s1.close();
            socket.close();
        }

        catch(Exception e) {
            System.out.println(e);
        }
    }
}
