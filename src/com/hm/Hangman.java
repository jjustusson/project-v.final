package com.hm;

/*
    Student names:  Andrew Brenycz
                    Julia Justusson
                    Robert Perez
                    Brittani Vinson

    Assignment:     FINAL PROJECT [File 3/5: Hangman]
 */

import java.awt.List;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;


public class Hangman implements Serializable {

    private String word;
    private String name;
    private String[] wordChoice;
    private StringBuffer dashed;//dashes
    protected int FailAttempts;//fail attempt counter
    protected String guess;
    protected boolean guessCorrect = false;
    @SuppressWarnings("unchecked")
    protected ArrayList<Character> guessed = new ArrayList();
    protected int Attempts = 5;
    protected int HowmanyLeft;
    GetWord getword = new GetWord();


    /**
     * Hangman: Creates an instance of Hangman object; begins and continues play of game.
     */
    public Hangman() {


        System.out.println("Welcome to Hangman " );

        BufferedReader typereader= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Choose a category:\n[1] movies\n[2] books\n" +
                           "...or select from:\n[3] CS\n[4] random (by difficulty level)\n\n");

        String category = null;
        String strChoice = null;
        int choice = 0;     // user-chosen category
        int dictionary = 0; // randomly chosen subcategory / dictionary

        while ((choice < 1) || (choice > 4)) {
            try {
                System.out.println("Enter your choice of some category number from 1-4: ");
                category = typereader.readLine();
                choice = Integer.parseInt(category);
            }
            catch (IOException e) {
                System.out.println("Try again; your choice must be an integer: ");
            }
        }


        if (choice == 1) {
            // movies
            strChoice = "movie";
            dictionary = new Random().nextInt(5);
        }

        else if (choice == 2) {
            // books
            strChoice = "written work";
            dictionary = new Random().nextInt(5);
        }

        else if (choice == 3) {
            // CS
            strChoice = "CS category";
            dictionary = new Random().nextInt(3);
        }

        else if (choice == 4) {
            // random difficulty
            strChoice = "difficulty level";
            dictionary = new Random().nextInt(5);
        }


        wordChoice = GetWord.getString(choice, dictionary);
        name = wordChoice[0];
        word = wordChoice[1];

        dashed = getword.makeDashes(word);
        getword.SetLength(word);
        HowmanyLeft = getword.getLengthLeft();


        System.out.println("Here is Word: " + word);
        System.out.format("Your word is from the %s: %s\n", strChoice, name);
        System.out.println("Length of word is: " + word.length());
        System.out.println("Dashed word: " + dashed);
        System.out.println("Attempts left " + Attempts );
        HangmanGraphics graphics = new HangmanGraphics();
        graphics.drawMan(Attempts);
        System.out.println("Enter Letter");
        Scanner reader = new Scanner(System.in);
        char letter = reader.next().charAt(0);
        checkLetter(letter);

        while (Attempts != 0) {

            HowmanyLeft = getword.getLengthLeft();
            if(HowmanyLeft == 0) {
                System.out.println("You win!");
                System.out.println("Thank you for playing");
                playagain();
            }

            System.out.println("Attempts Left: " + Attempts);
            System.out.println("Dashes left: " +HowmanyLeft);
            graphics.drawMan(Attempts);
            System.out.println("Enter Letter");
            reader = new Scanner(System.in);
            letter = reader.next().charAt(0);
            checkLetter(letter);
        }

        if(Attempts == 0) {
            graphics.drawMan(0);
            System.out.println("You lose, word was: " + word);
            playagain();
        }
    }

    /**
     * Determines if the user would like to play again.
     *  Exits the game or begins another round, as appropriate.
     */
    public void playagain() {

        System.out.println("Would you like to play again? (Y/N)");

        Scanner reader = new Scanner(System.in);
        reader = new Scanner(System.in);
        char again = reader.next().charAt(0);
        char f = Character.toUpperCase(again);
        char y = 'Y';
        char n = 'N';

        if(f != y && f != n) {
            System.out.println("Not valid respnose! ");
            playagain();
        }

        if(f == 'Y') {
            Hangman hang = new Hangman();
            System.out.println("Yes");
        }
        else if (f == 'N') {
            System.out.println("Closing Game");
            System.exit(0);
        }
    }


    /**
     * Determines if user input [letter] is contained within the word to be guessed.
     *  Checks for and handles repeat guesses (not counted against user).
     *
     * @param letter -- the user-input letter, which may or may not be part of the word
     */
    public void checkLetter(char letter) {

        if(!guessed.contains(letter)) {
            guessed.add(letter);
            int i = 0;
            int j = 0;
            i = word.indexOf(letter);
            j = word.indexOf(Character.toUpperCase(letter));
            if (i != -1 || j != -1) {
                System.out.println("found ");
                GetWord.matchLetter(word, dashed, letter);
            }
            else {
                Attempts -= 1;
                System.out.println("Not found ");
            }
        }
        else {
            System.out.println("You guessed Letter already, pick different letter");
        }
    }


    /**
     * @return -- Returns the dashed (all-hyphen) representation of the word to be guessed.
     */
    public StringBuffer getDashed() {
        return dashed;
    }

    /**
     * @return -- Returns the word to be guessed by the user.
     */
    public String getWord() {
        return  word;
    }
}

