package com.hm;

/*
    Student names:  Andrew Brenycz
                    Julia Justusson
                    Robert Perez
                    Brittani Vinson

    Assignment:     FINAL PROJECT [File 4/5: GetWord]
 */

import java.util.ArrayList;
import java.util.Random;

/**
 * GetWord: Selects a word to be guessed in current Hangman game.
 */
public class GetWord {

    // ------------------------- opt [1] : BOOK dictionaries -------------------------

    // Hitchhiker's Guide to the Galaxy
    private static String[] wordsHGTG = { "towels", "Zaphod", "Trillian", "Marvin", "paranoid",
            "android", "Hactar", "supercomputer", "Arthur", "Slartibartfast", "Roosta", "Zarniwoop",
            "Vogons", "Magrathea", "Milliways", "Nutrimatic", "apocalypse", "ultimate", "answer",
            "restaurant", "universe", "improbability", "question", "everything", "power",
            "lightyears", "galactic", "planets", "Cybernetics", "transporter", "starship",
            "Golgafrincham", "infinite", "bistromathics", "hyperspace", "supernova", "catastrophe",
            "million", "primitive", "insignificant", "consequences", "dimension", "possibility",
            "calculator", "complicated", "hyperspatial", "destination", "adventurer", "ridiculous",
            "government", "interstellar", "microprocessor", "equipment", "metaphysical" };

    // Game of Thrones
    private static String[] wordsGOT = { "Eddard", "Catelyn", "Lannister", "Daenerys", "Viserys",
            "Targaryen", "execution", "kingdoms", "Winterfell", "direwolves", "Baelish", "power",
            "treason", "winter", "whitewalker", "wilderness", "wildling", "wights", "loyalty",
            "dragons", "Dothraki", "allegiance", "Westeros", "Baratheon", "Stormlands",
            "independent", "Valyria", "marriage", "bastard", "conqueror", "warfare", "bloodshed",
            "shadowcats", "assassination", "murder", "castle", "Stormlands", "throne", "poison",
            "guardsmen", "Dothraki", "mysterious", "frightening", "destruction", "realm",
            "skinchanger", "telepathy", "incest", "bastard", "massacre", "rebellion", "kidnapping",
            "dangerous", "blackmail" };

    // Frankenstein
    private static String[] wordsF = { "Victor", "Robert", "Margaret", "Walton", "explorer",
            "repulsion", "monster", "father", "Germany", "Elizabeth", "night", "death", "miserable",
            "repulsion", "experiment", "Justine", "despair", "happiness", "horror", "creature",
            "Geneva", "countenance", "affection", "knowledge", "sensation", "appearance",
            "companion", "spirits", "existence", "science", "murderer", "beautiful", "anguish",
            "brother", "electronic", "Henry", "wretched", "revenge", "murder", "natural",
            "destruction", "heaven", "creator", "innocence", "vengeance", "melancholy", "victims",
            "misfortune", "philosophy", "horrible", "solitude", "suffering", "tranquility",
            "agitation", "mankind", "corpse", "hideous", "wilderness", "agitation" };

    // Sherlock Holmes
    private static String[] wordsSH = { "physician", "Watson", "friendship", "adventure",
            "detective", "Hudson", "eccentric", "tobacco", "cigarettes", "cocaine", "intellectual",
            "peculiarity", "revolver", "pistol", "investigation", "government", "assassin",
            "reasoning", "inferences", "disguise", "Gutenburg", "fencing", "observation",
            "knowledge", "psychology", "elementary", "mystery", "laboratory", "speculation",
            "telegram", "poison", "handcuffs", "gentleman", "companion", "death", "inspector",
            "remember", "photograph", "interesting", "remarkable", "peculiar", "evidence",
            "innocence", "confession", "information" };

    // 1984
    private static String[] words1984 = { "thoughtcrime", "doublethink", "history", "revisionism",
            "Winston", "dystopian", "superstate", "Ministry", "committee", "proletarian", "Oceania",
            "telescreen", "Newspeak", "Oldspeak", "Ingsoc", "principles", "brother", "brotherhood",
            "ministry", "impossible", "Goldstein", "reality", "hierarchical",  "society",
            "prisoners", "political", "rebellion", "revolution", "existence", "consciousness",
            "knowledge", "universal", "suppression", "oppression", "understanding", "expression",
            "language", "intellectual", "principles", "vaporized", "ideological", "individualism",
            "criminals", "destruction", "frightening", "community", "creature", "conspiracy",
            "capitalism", "socialism", "totalitarianism", "collectivism", "omnipresence",
            "government", "surveillance", "scrutiny", "uniformity", "microphones", "documents",
            "photographs", "manipulation", "power", "corruption", "propaganda", "interrogation",
            "confession", "operation", "electroshock", "conviction", "treason", "demoralization" };

    // ------------------------- opt [2] : MOVIE dictionaries -------------------------

    // Star Wars
    private static String[] wordsSW = { "Palpatine", "Kenobi", "Skywalker", "Anakin", "Chewbacca",
            "Amidala", "Grievous", "Padawan", "apprentice", "Alderaan", "Dagobah", "Tatooine",
            "Mustafar", "galaxy", "lightsaber", "universe", "Force", "allegiance", "alliance",
            "democracy", "citizenship", "dictatorship", "Republic", "Imperial", "battleships",
            "prophecy", "destruction", "hologram", "training", "hyperspace", "hyperdrive",
            "telepathic", "starfighters", "father", "partnership", "Rebellion", "Federation",
            "podracer", "chancellor", "starships", "friendship", "assassination", "superweapon",
            "power", "representative", "commander", "devastation", "challenge", "treachery" };

    // Monty Python and the Holy Grail
    private static String[] wordsMP = { "Arthur", "Bedevere", "Lancelot", "Galahad", "Robin",
            "Patsy", "Gawain", "Camelot", "knights", "castle", "rabbit", "horseback", "quest",
            "monks", "rabbit", "perilous", "wedding", "Caerbannog", "monks", "questions",
            "witches", "historian", "guard", "father", "villager", "bravery", "singing", "dingo",
            "mortician", "shrubbery", "minstrel", "Britons", "brother", "grenade", "forest" };

    // Hackers
    private static String[] wordsH = { "computers", "conviction", "intrusion", "photographic",
            "memory", "disconnected", "fragmented", "dangerous", "supercomputer", "mainframe",
            "rainstorm", "security", "unauthorized", "confistication", "supremacy", "evidence",
            "transactions", "excitement", "passwords", "terminal", "disruption", "exploitation",
            "informant", "interrogation", "override", "Babbage" };

    // 2001: A Space Odyssey
    private static String[] words2001 = { "evolution", "humankind", "monolith", "mysterious",
            "epidemic", "cryogenic", "hibernation", "scientists", "Discovery", "spacecraft",
            "Jupiter", "sentient", "supercomputer", "astronauts", "oxygen", "rescue", "jeopardy",
            "emergency", "airlock", "processor", "disconnect", "existence", "existentialism",
            "emission", "investigation", "vortex", "cosmological", "phenomena", "spacesuit",
            "shuttle", "mission", "controller", "extraterrestrial", "superintelligence", "universe",
            "mystery", "remoteness", "limitless", "immortality", "satellites", "breakdown",
            "hardware", "broadcast", "civilization", "anxiety" };

    // The Big Lebowski
    private static String[] wordsTBL = { "opinion", "dude", "Walter", "Vietnam", "veteran",
            "bowling", "teammate", "slacker", "deadbeat", "marijuana", "wheelchair", "millionaire",
            "philanthropy", "heirloom", "narration", "disapproval", "nihilists", "musicians",
            "kidnapping", "league", "semifinals", "mistaken", "identity", "commission", "ransom",
            "Treehorn", "briefcase", "unconscious", "laundry", "foundation", "extortion" };

    // ------------------------- opt [3] : COMPUTER SCIENCE dictionaries -------------------------

    // CS: math
    private static String[] wordsCSM = { "Pythagoras", "Fermat", "Galois", "combinatorics",
            "encryption", "boolean", "recursion", "induction", "cardinality", "ordinality",
            "paradox", "completeness", "isomorphism", "continuum", "hypothesis", "randomness",
            "enumerable", "deduction", "complexity", "singleton", "countability", "intersection",
            "hierarchy", "automata", "quadratic", "optimization", "discretization", "polynomial",
            "equilibrium", "Fibonacci", "pigeonhole", "triangulation", "logarithm", "factorization",
            "clustering", "exponential" };

    // CS: algorithms
    private static String[] wordsCSA = { "Euclid", "axiom", "pseudocode", "transition",
            "implementation", "complexity", "simulation", "Turing", "proposition", "induction",
            "recursion", "heuristic", "sequential", "optimization", "Hanoi", "paradigm",
            "deduction", "deterministic", "quantum", "randomized", "polynomial", "reduction",
            "memoization", "dynamic", "approximation", "Fourier", "pseudorandom", "subsequence",
            "topological", "Kruskal", "transitive", "Dijkstra", "binary" };

    // CS: distributed systems(!) / networking
    private static String[] wordsCSDS = { "synchronous", "communication", "hostname", "sockets",
            "transmission", "operations", "configuration", "telecommunication", "allocation",
            "asynchronous", "packets", "infrastructure", "domain", "database", "fragmentation",
            "destination", "sequence", "streaming", "execution", "concurrency", "multithreading",
            "parameter", "security", "administrator", "maintenance", "architecture", "transparency",
            "referencing", "compatibility", "protocols", "datagram", "accessibility", "dynamically",
            "allocation", "representation", "implementation", "transaction" };

    // ------------------------- opt [4] : RANDOM DIFFICULTY dictionaries -------------------------

    // difficulty depends on word length (shorter is presumably easier):
    //   each dictionary contains 20 words of identical length-- 7, 8, 9, 10, or 11 letters

    private static String[] words7 = { "plainly", "rebrand", "balloon", "shutoff", "leather",
                                       "elusive", "pungent", "mutedly", "lighten", "padlock",
                                       "withers", "popcorn", "vehicle", "apology", "seaside",
                                       "papyrus", "lettuce", "exhibit", "lyrical", "aqueous" };

    private static String[] words8 = { "chipmunk", "disciple", "syllable", "cajoling", "epiphany",
                                       "lollipop", "alienate", "unsteady", "tetrarch", "archival",
                                       "pontific", "ligament", "abnormal", "sneakily", "downfall",
                                       "charisma", "finality", "stagnant", "antibody", "emulsion" };

    private static String[] words9 = { "injustice", "cupholder", "sugarplum", "beanstalk",
                                       "involuted", "occultist", "humectant", "formalize",
                                        "disbelief", "beekeeper", "luminesce", "quadratic",
                                        "justified", "laborious", "cloudlike", "enunciate",
                                        "physician", "criticize", "observant", "historian" };

    private static String[] words10 = { "discussion", "fruitfully", "unfunereal", "irrigating",
                                        "volatilize", "ineptitude", "collarbone", "monotonous",
                                        "neutrophil", "priestlier", "rejuvenate", "preincline",
                                        "compendium", "nonemulous", "knobkerrie", "shrewdness",
                                        "preworship", "theomaniac", "innervated", "polygenism" };

    private static String[] words11 = { "millisecond", "hyperaction", "superstruct", "qualitative",
                                        "bimolecular", "crystalline", "hyperbolize", "fascination",
                                        "microcosmic", "mandolinist", "zebrafishes", "pterodactyl",
                                        "kinesthesia", "juxtaposing", "yachtswomen", "filamentous",
                                        "hibernation", "foreshorten", "isoelectric", "oxychloride" };


    private String secretWord;       // the chosen secret word
    private ArrayList<Character> correctLetters;   // correct guesses
    private ArrayList<Character> incorrectLetters; // incorrect guesses
    private static int dashesLeft;
    char letter;

    /**
     * Gets the word to be guessed from the appropriate category.
     *  (word picked from randomly chosen dictionary within that category)
     *
     * @param category -- the category that the user selected
     * @param dict -- a specific dictionary within the selected category
     * @return -- Returns the name of the particular dictionary (as a sort of hint) and the chosen word
     */
    public static String[] getString(int category, int dict) {

        String[] words = new String[0];
        String name = null;

        if (category == 1) { // word chosen from one of the movie dictionaries
            if (dict == 0) {
                words = wordsSW;
                name = "the Star Wars saga";
            }
            else if (dict == 1) {
                words = wordsMP;
                name = "Monty Python and the Holy Grail";
            }
            else if (dict == 2) {
                words = wordsH;
                name = "Hackers";
            }
            else if (dict == 3) {
                words = words2001;
                name = "2001: A Space Odyssey";
            }
            else if (dict == 4) {
                words = wordsTBL;
                name = "The Big Lebowski";
            }
        }
        else if (category == 2) { // word chosen from a book dictionary
            if (dict == 0) {
                words = wordsHGTG;
                name = "The Hitchhiker's Guide to the Galaxy";
            }
            else if (dict == 1) {
                words = wordsGOT;
                name = "the Game of Thrones saga";
            }
            else if (dict == 2) {
                words = wordsF;
                name = "Frankenstein";
            }
            else if (dict == 3) {
                words = wordsSH;
                name = "the canon of Sherlock Holmes";
            }
            else if (dict == 4) {
                words = words1984;
                name = "George Orwell's 1984";
            }
        }
        else if (category == 3) { // word chosen from a computer science dictionary
            if (dict == 0) {
                words = wordsCSM;
                name = "Mathematics";
            }
            else if (dict == 1) {
                words = wordsCSA;
                name = "Algorithms";
            }
            else if (dict == 2) {
                words = wordsCSDS;
                name = "Distributed Systems";
            }
        }
        else if (category == 4) { // word chosen from a random word-length dictionary
            if (dict == 0) {
                words = words7;
                name = "7-letter words";
            }
            else if (dict == 1) {
                words = words8;
                name = "8-letter words";
            }
            else if (dict == 2) {
                words = words9;
                name = "9-letter words";
            }
            else if (dict == 3) {
                words = words10;
                name = "10-letter words";
            }
            else if (dict == 4) {
                words = words11;
                name = "11-letter words";
            }
        }

        int index = new Random().nextInt(words.length);
        String randomWord = words[index];
        String[] details = new String[]{name, randomWord};
        return details;
    }

    public void SetLength(String word) {
        dashesLeft = word.length();
    }

    public static void removeLength() {
        dashesLeft -=1;
    }

    public int getLengthLeft() {
        return dashesLeft;
    }

    /**
     * If the letter is a match in the word, change display of word to reflect this
     *  and output message to user.
     *
     * @param secret -- the word that is being guessed
     * @param dashes -- the un-guessed letters in the word [secret]
     * @param letter -- the user-input letter guessed as part of the word [secret]
     */
    public static void matchLetter(String secret, StringBuffer dashes, char letter) {

        for (int index = 0; index < secret.length(); index++) {
            if (secret.charAt(index) == letter) {
                removeLength();
                dashes.setCharAt(index, letter);
            }
            if (secret.charAt(index) == Character.toUpperCase(letter)) {
                removeLength();
                dashes.setCharAt(index, Character.toUpperCase(letter));
            }
        }
        System.out.println("good guess : " + dashes);
    }

    /**
     * Generates the display of word to be guessed, with each letter represented by a hyphen.
     *
     * @param s -- the word to be guessed
     * @return -- returns game display of word [dashes]
     */
    public static StringBuffer makeDashes(String s) {

        StringBuffer dashes = new StringBuffer(s.length());
        for (int count=0; count < s.length(); count++)
            dashes.append('-');
        return dashes;
    }
}

