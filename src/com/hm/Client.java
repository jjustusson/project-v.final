package com.hm;

/*
    Student names:  Andrew Brenycz
                    Julia Justusson
                    Robert Perez
                    Brittani Vinson

    Assignment:     FINAL PROJECT [File 1/5: Client]
 */

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


public class Client {

    /**
     * Client: Initializes new instance of Hangman, connects client to server.
     */
    public static void main(String[] args) {

        System.out.println();

        try {

            Hangman hang = new Hangman();

            Socket s = new Socket("127.0.0.1", 1236);

            ObjectOutputStream clientOutputStream = new
                    ObjectOutputStream(s.getOutputStream());

            ObjectInputStream clientInputStream = new
                    ObjectInputStream(s.getInputStream());

            clientOutputStream.writeObject(hang);

            hang = (Hangman)clientInputStream.readObject();

            clientOutputStream.close();
            clientInputStream.close();
            s.close();
        }

        catch(Exception e) {
            System.out.println(e);
        }
    }
}

